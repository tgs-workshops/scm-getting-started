# Source Code Management - Getting Started
In this workshop, we'll walk you through:
- Why use Source Code Management
- Prepare Workstations
- Set up a GitLab account
- Generate your ssh keypair and uploading the public key to GitLab
- Publish a repository
- Create a feature branch for your repository
- Test a change
- Create a Merge request
- Peer review a proposed change
- Merge to master

# Preparing your workstation environment
## Prepare Windows Workstation Using Chocolatey

1. Open Powershell Admin Prompt
2. [Install Chocolatey](https://chocolatey.org/install) package manager to streamline application installs
    - Copy and Execute command in PS
3. Install *Git*    -  `cinst git -y`
4. Install *VSCode*   -  `cinst vscode -y`

## Prepare Windows Workstation Using Standard Installers
1. Install *Git for Windows* manually with [Git for Windows Installers](https://git-scm.com/download/win)
2. Install *VSCode* manually with [VSCode Installers](https://code.visualstudio.com/download)

## Prepare your MacOS Workstation
1. (Optional) Install Brew package manager to streamline application installs - [Install Brew](https://brew.sh/)
2. (Optional) Install *Git for MacOS* via `brew install git` (Brew) [^1]
3. Install *VSCode* via `brew install visual-studio-code` (Brew) or manually with [VSCode Installers](https://code.visualstudio.com/download)

# Getting Started with GitLab
## Create a GitLab Account
- Navigate to [Gitlab.com](https://gitlab.com)
- Fill out the form and click Register
## Creating your SSH keypair
1. Open your command prompt
2. Navigate to ~/.ssh
        ssh-keygen -t ed25519 -C "<comment>"
3. Enter a file name or accept default
        ...key (C:\Users\<username>/.ssh/id_ed25519)
4. Press Enter for no passphrase
5. Marvel at your newly created `ed25519` keypair

*Note:* 
- *The book Practical Cryptography With Go suggests that ED25519 keys are more secure and performant than RSA keys, therefore are our recommendation. If you don't want to leverage ED25519 keys, you can replace ed25519 with rsa and add -b 2048.*
- *If you provide a passphrase the built-in vscode solution will not cooperate with the need for a passphrase.*

## Configure GitLab with your public key
1. Copy the contents of your public key to your clipboard
    - `cat ~/.ssh/<keyname>.pub | clip`
    - **OR** Open <keyname>.pub in vscode and copy contents  
2. Navigate to User Settings/SSH Keys
3. Paste clipboard into the Key box
4. Provide a common name and/or expiration date, if desired
5. Click add key
6. Test your key's access to GitLab with the following command:
    - `ssh -T -i <privatekey> git@gitlab.com`
7. Success when you see the following
    - `Welcome to GitLab, @<username>!`

## Initializing a git repository from gitlab.com
1. Navigate to Gitlab.com
2. Click New project
3. Give your project a name
4. Provide a Project Description
5. Set Visibility Level
6. Check Initialize repository with a README
7. Click Create project

## Publishing a project folder to GitLab repository
*UNDER CONSTRUCTION*
## Creating a feature branch
- `git checkout -b "feature-branch-name"`

## Committing a change to the feature branch
*UNDER CONSTRUCTION*

##






















[^1] MacOS ships with an older version of Git which may function just fine; Brew installing ensures you have the latest version.
