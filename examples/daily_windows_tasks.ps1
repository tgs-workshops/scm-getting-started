$ErrorActionPreference = "Continue"
$ConfirmPreference = "None"

# update/install powershell modules
@(
Get-PSRepository | Set-PSRepository -InstallationPolicy Trusted

$modules = "Chocolatey","VMware.PowerCLI","VMware.VimAutomation.HorizonView","PowerNSX","PowervRA","PowervRO","PowervLCM","VMware.VMC","Vester","POSH-SSH","POSH-Git","PSWindowsUpdate","PackageManagement","NuGet","PowerShellGet","Pester","OperationValidation","Microsoft.Powershell.Archive","Microsoft.Powershell.Odatautils","AWSPowerShell","Azure"

ForEach ( $module in $modules ) {
    If ( !( Get-Module -ListAvailable $module ) ) {
        Install-Module -Name $module �Force -AllowClobber -SkipPublisherCheck
    }
    Else {
        Try {
            Update-Module -Name $module -ErrorAction Stop
        }
        Catch {
            Install-Module -Name $module �Force -AllowClobber -SkipPublisherCheck
        }
    }

    $versions = Get-Module -ListAvailable $module
    While ( $versions.count -gt 1 ) {
        $extraModule = $versions | Sort-Object -Property Version | Select-Object -First 1
        $modulePath = ( Get-Item -Path $extraModule.Path ).Directory.FullName
        takeown /F $modulePath /A /R
        icacls $modulePath /reset /C /Q
        icacls $modulePath /grant Administrators:'F' /inheritance:d /T /C /Q
        Remove-Item -Path $modulePath -Recurse -Force -ErrorAction SilentlyContinue

        $versions = Get-Module -ListAvailable $module
    }
}


$modules = Get-Module -ListAvailable | Where-Object { $_.Path -like "*C:\Program Files\WindowsPowerShell\Modules*" }

ForEach ( $module in $modules ) {
    $versions = Get-Module -ListAvailable $module.Name
    While ( $versions.count -gt 1 ) {
        $extraModule = $versions | Sort-Object -Property Version | Select-Object -First 1
        $modulePath = ( Get-Item -Path $extraModule.Path ).Directory.FullName
        takeown /F $modulePath /A /R
        icacls $modulePath /reset /C /Q
        icacls $modulePath /grant Administrators:'F' /inheritance:d /T /C /Q
        Remove-Item -Path $modulePath -Recurse -Force -ErrorAction SilentlyContinue

        $versions = Get-Module -ListAvailable $module
    }
}

Get-Module -ListAvailable | Where-Object { $_.Path -like "*C:\Program Files\WindowsPowerShell\Modules*" } | Select-Object Name,Version,Path | Format-Table -Autosize
) # end updating/installing powershell modules

# update/install programs through chocolatey
@(
choco feature enable -n allowGlobalConfirmation

$chocoPackages = "dotnet4.7.2", "openjdk13", "vcredist-all", "powershell", "notepadplusplus", "uBlockOrigin-chrome", "googlechrome", "firefox", "uBlockOrigin-firefox", "telnet", "7zip", "putty", "git", "winscp", "vscode", "vscode-powershell", "vscode-icons", "vscode-docker", "vscode-mssql", "vscode-editorconfig", "vscode-autofilename", "vscode-gitattributes", "vscode-gitlens", "vscode-autohotkey", "vscode-ansible", "chocolatey-vscode", "vscode-settingssync", "teamspeak", "rdcman", "conemu", "freefilesync", "google-backup-and-sync", "python3", "windirstat", "postman", "insomnia-rest-api-client", "MobaXTerm", "Microsoft-Teams", "Microsoft-Edge"

choco upgrade -y --no-progress --acceptlicense --limitoutput --ignoredetectedreboot --install-if-not-installed ( $chocoPackages | Sort-Object )

Get-ChildItem -Path C:\Users\Public\Desktop | Remove-Item -Confirm:$false
Get-ChildItem -Path $env:USERPROFILE\Desktop | Remove-Item -Confirm:$false
) # end updating/installing programs through chocolatey

# install windows updates
@(
Import-Module PSWindowsUpdate
If ( !( Get-WUServiceManager -ServiceID 7971f918-a847-4430-9279-4a52d1efe18d ) ) {
    Add-WUServiceManager -ServiceID 7971f918-a847-4430-9279-4a52d1efe18d
}

$outputCheck = Get-WindowsUpdate -MicrosoftUpdate -Title "Silverlight" -Hide -Confirm:$false
$outputCheck += Get-WindowsUpdate -WindowsUpdate -Title "Silverlight" -Hide -Confirm:$false
While ( $outputCheck -ne $null ) {
    $outputCheck = Get-WindowsUpdate -MicrosoftUpdate -Title "Silverlight" -Hide -Confirm:$false
    $outputCheck += Get-WindowsUpdate -WindowsUpdate -Title "Silverlight" -Hide -Confirm:$false
}

Get-WUInstall �MicrosoftUpdate �AcceptAll -RecurseCycle 2 -Install -IgnoreReboot -IsHidden:$false -NotTitle "Silverlight"
) # end installing windows updates

# run dism and disk cleanup
@(
dism /online /cleanup-image /spsuperseded
dism /online /cleanup-image /StartComponentCleanup /ResetBase

#C:\windows\system32\cleanmgr.exe /sagerun:1
) # end running dism and disk cleanup

# setup storagesense
@(
$key = "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\StorageSense"
If (!(Test-Path "$key")) {
    New-Item -Path "$key" | Out-Null
}
If (!(Test-Path "$key\Parameters")) {
    New-Item -Path "$key\Parameters" | Out-Null
}
If (!(Test-Path "$key\Parameters\StoragePolicy")) {
    New-Item -Path "$key\Parameters\StoragePolicy" | Out-Null
}

# Set Storage Sense settings
# Enable Storage Sense
Set-ItemProperty -Path "$key\Parameters\StoragePolicy" -Name "01" -Type DWord -Value 1

# Set 'Run Storage Sense' to Every Week
Set-ItemProperty -Path "$key\Parameters\StoragePolicy" -Name "2048" -Type DWord -Value 1

# Enable 'Delete temporary files that my apps aren't using'
Set-ItemProperty -Path "$key\Parameters\StoragePolicy" -Name "04" -Type DWord -Value 1

# Set 'Delete files in my recycle bin if they have been there for over' to 14 days
Set-ItemProperty -Path "$key\Parameters\StoragePolicy" -Name "08" -Type DWord -Value 1
Set-ItemProperty -Path "$key\Parameters\StoragePolicy" -Name "256" -Type DWord -Value 1

# Set 'Delete files in my Downloads folder if they have been there for over' to 60 days
Set-ItemProperty -Path "$key\Parameters\StoragePolicy" -Name "32" -Type DWord -Value 0
Set-ItemProperty -Path "$key\Parameters\StoragePolicy" -Name "512" -Type DWord -Value 1

# Set value that Storage Sense has already notified the user
Set-ItemProperty -Path "$key\Parameters\StoragePolicy" -Name "StoragePoliciesNotified" -Type DWord -Value 1
) # end setting up storagesense
