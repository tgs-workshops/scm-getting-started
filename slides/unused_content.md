---

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Clone the repo
</span>

        git clone <git_path_to_repo>

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Create a feature branch for your repository
</span>

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<!-- <span style="font-family:Ubuntu Mono; font-size:30px;"> -->
<!-- <section style="text-align: left;"> -->

        git checkout -b "new-branch-name"

<!-- <section style="text-align: center;"> -->

<!-- ![GitLab Registration](/media/gitlab_registration.png) -->
</span>

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Ubuntu Mono; font-size:30px;">
<section style="text-align: left;">

- Navigate to Repository/Branches
- Click New Branch
- Now checkout the new branch
         git checkout "new-branch-name"

<section style="text-align: center;">

![GitLab New Branch](/media/gitlab_new_branch.png)
</span>

<!-- ---

<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Test a change
</span>

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->

<span style="font-family:Ubuntu Mono; font-size:30px;">
<section style="text-align: left;">

- How do we want to show this?

<section style="text-align: center;">

<!-- ![GitLab Registration](/media/gitlab_registration.png) -->
</span>

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Create a Merge request    
</span>

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->

<span style="font-family:Ubuntu Mono; font-size:30px;">
<section style="text-align: left;">

- In your browser navigate to Merge Requests
- Click New Merge Request
- Select your working/source branch and the Target branch

<section style="text-align: center;">


![GitLab Merge Request](/media/gitlab_new_merge_request.png)

</span>

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Peer review a proposed change
</span>


----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Ubuntu Mono; font-size:30px;">
<section style="text-align: left;">

- Placeholder text

<section style="text-align: center;">

![GitLab Registration](/media/gitlab_registration.png)
</span>

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@800; font-size:50px;">
Merge to master
</span>

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Ubuntu Mono; font-size:30px;">
<section style="text-align: left;">

- Placeholder text

<section style="text-align: center;">

![GitLab Registration](/media/gitlab_registration.png)
</span>  -->