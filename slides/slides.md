---
title: SCM Getting started
theme: white
revealOptions:
    transition: 'convex'
---

<!-- .slide: data-background="./media/tgs_background_title.png" -->
&nbsp;\
\
&nbsp;

<span style="font-family:Rokkitt:wght@800; font-size:50px;">
Source Code Management

Getting started
</span>

\
<span style="font-family:Ubuntu Mono; font-size:30px;">
Developed and Presented by
\
Dustin Holub & Nathan Thomas

</span>

Note: 
- Links to copy and paste into webex when appropriate follow:
- Install Chocolatey - https://chocolatey.org/install
- Install choco script: Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
- Cinst git / vscode - cinst git vscode -y
- create a gitlab accont: https://gitlab.com
- 
----
<!-- .slide: data-background="./media/dth_bio.png" -->

----
<!-- .slide: data-background="./media/nat_bio.png" -->

---

<!-- .slide: data-background="./media/tgs_background_tgs_logo.png" -->
<span style="font-family:Rokkitt:wght@800; font-size:50px;">
Agenda 
</span>

<span style="font-family:Ubuntu Mono; font-size:30px;">

- Why use Source Code Management
- Prepare Workstations
- Set up a GitLab account
- Generate your ssh keypair and uploading the public key to GitLab
- Publish a repository
- Create a feature branch for your repository
- Test a change
- Create a Merge request
- Peer review a proposed change
- Merge to master

</span>

---
<!-- .slide: data-background="./media/tgs_background_tgs_logo.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Why Source Code Management 
</span>

<span style="font-family:Ubuntu Mono; font-size:30px;">

Increase Visibility  
Increase Reliability  
Lay Foundation for Automation  

</span>

![GitFlow](/media/git_flow.png)

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Prepare Windows Workstation Using Chocolatey
</span>

<span style="font-family:Ubuntu Mono; font-size:30px;">

1. Open Powershell Admin Prompt

2. [Install Chocolatey](https://chocolatey.org/install) package manager to streamline application installs
    - Copy and Execute command in PS

3. Install *Git*
        cinst git -y

4. Install *VSCode*
        cinst vscode -y

</span>

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Prepare Windows Using Standard Installers
</span>

<span style="font-family:Ubuntu Mono; font-size:30px;">

1. Install *Git for Windows* manually with [Git for Windows Installers](https://git-scm.com/download/win)
2. Install *VSCode* manually with [VSCode Installers](https://code.visualstudio.com/download)

</span>

---
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Create a Gitlab Account
</span>
<!-- .slide: data-background="./media/tgs_background_blank.png" -->

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<section style="text-align: left;">
<span style="font-family:Ubuntu Mono; font-size:30px;">

- Navigate to [Gitlab.com](https://gitlab.com)
- Fill out the form and click Register

</span>
<section style="text-align: center;">

![GitLab Registration](/media/gitlab_registration.png)

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Generate SSH Key & Add SSH Key to GitLab
</span>

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Ubuntu Mono; font-size:30px;">

1. Open your command prompt
2. Navigate to ~/.ssh
        ssh-keygen -t ed25519 -C "<comment>"
3. Enter a file name or accept default
        ...key (C:\Users\<username>/.ssh/id_ed25519)
4. Press Enter for no passphrase
5. Marvel at your newly created `ed25519` keypair

Note: 
- The book Practical Cryptography With Go suggests that ED25519 keys are more secure and performant than RSA keys, therefore are our recommendation. If you don't want to leverage ED25519 keys, you can replace ed25519 with rsa and add -b 2048.
- If you provide a passphrase the built-in vscode solution will not cooperate with the need for a passphrase.
</span>

----

<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Ubuntu Mono; font-size:30px;">

1. Copy the contents of your public key to your clipboard
        cat ~/.ssh/<keyname>.pub | clip
      Open <keyname>.pub in vscode and copy contents  
2. Navigate to User Settings/SSH Keys
3. Paste clipboard into the Key box
4. Provide a common name and/or expiration date, if desired
5. Click add key
6. Test your key's access to GitLab with the following command:
        ssh -T -i <privatekey> git@gitlab.com
7. Success when you see the following
        Welcome to GitLab, @<username>!

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Initialize a git repo
</span>

----
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Ubuntu Mono; font-size:30px;">

1. Navigate to [Gitlab.com](https://gitlab.com)
2. Click New project
3. Give your project a name
4. Provide a Project Description
5. Set Visibility Level
6. Check Initialize repository with a README
7. Click Create project
8. Click Clone
9. Click clipboard under "Clone with SSH"

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
**Clone the repo**

        git clone <git_path_to_repo>

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
**Create a feature branch for your repository**

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
**Make a few changes and commit / sync**

---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
**Create a Merge request**         


---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Rokkitt:wght@600; font-size:50px;">
Summary
</span>

<span style="font-family:Ubuntu Mono; font-size:30px;">

- Why use Source Code Management
- Prepare Workstations
- Set up a GitLab account
- Generate your ssh keypair and uploading the public key to GitLab
- Publish a repository
- Create a feature branch for your repository
- Test a change
- Create a Merge request
- Peer review a proposed change
- Merge to master
</span>
---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
**Q & A**


---
<!-- .slide: data-background="./media/tgs_background_blank.png" -->
<span style="font-family:Ubuntu Mono; font-size:60px;">
The End.
</span>